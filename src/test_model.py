import pickle
import pandas as pd
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

# Load the test dataset
test_data = pd.read_csv('data/preprocessed/test_data.csv')

# Load the trained model
with open('models/log_reg.pkl', 'rb') as f:
    model = pickle.load(f)

# Separate features and target
X_test = test_data.drop('target', axis=1)
y_test = test_data['target']

# Make predictions
y_pred = model.predict(X_test)

# Calculate and print metrics
accuracy = accuracy_score(y_test, y_pred)
precision = precision_score(y_test, y_pred)
recall = recall_score(y_test, y_pred)
f1 = f1_score(y_test, y_pred)

print(f'Accuracy: {accuracy}')
print(f'Precision: {precision}')
print(f'Recall: {recall}')
print(f'F1 Score: {f1}')
